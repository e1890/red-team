#!/bin/bash

echo "Select Correct Option"

echo "1: Banner Grabbing for HTTP/HTTPS"
echo "2: Banner Grabbing for Other Services"
echo ""
echo "Your Option:"

read option

if [ $option == 1 ]
then
	echo "Enter URL"
read URL

#curl_output=$(curl -s -I $URL | grep "Server:"| cut -d ":" -f 2)

cmd=HEAD / HTTP/1.0

output_nc=$(echo -e $cmd \r\n| nc $URL 80| grep "Server:"| cut -d " " -f 2)

echo "Your Server is : $output_nc"

elif [ $option == 2 ]
then	
	echo "Enter IP"
	read IP
	echo "Enter Port"
	read PORT
	nmap_server=$(nmap -sV -p $PORT $IP| grep "/tcp"| cut -d " " -f 9)
	nmap_server_version=$(nmap -sV -p $PORT $IP| grep "/tcp" | cut -d " " -f 10)
	echo "Service Details for $PORT : $nmap_server"
	echo "Service Version for $PORT : $nmap_server_version"

else 
	echo "Enter Valid Input"

fi

